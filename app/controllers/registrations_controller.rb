class RegistrationsController < Devise::RegistrationsController

  private
  def sign_up_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation, :cpf, :public_place, :district, :number_house, :complement, :cep, :city, :state)
  end

  def account_update_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation, :current_password, :cpf, :public_place, :district, :number_house, :complement, :cep, :city, :state, :bust, :waist, :hip)
  end
end
