class ClotesController < ApplicationController
  before_action :set_clote, only: [:show, :edit, :update, :destroy]

  # GET /clotes
  # GET /clotes.json
  def index
    @clotes = Clote.all
  end

  # GET /clotes/1
  # GET /clotes/1.json
  def show
  end

  # GET /clotes/new
  def new
    @clote = Clote.new
  end

  # GET /clotes/1/edit
  def edit
  end

  # POST /clotes
  # POST /clotes.json
  def create
    @clote = Clote.new(clote_params)


    respond_to do |format|
      if @clote.save
        format.html { redirect_to @clote, notice: 'Modelo salvo com sucesso.' }
        format.json { render :show, status: :created, location: @clote }
      else
        format.html { render :new }
        format.json { render json: @clote.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /clotes/1
  # PATCH/PUT /clotes/1.json
  def update
    respond_to do |format|
      if @clote.update(clote_params)
        format.html { redirect_to @clote, notice: 'Modelo salvo com sucesso.' }
        format.json { render :show, status: :ok, location: @clote }
      else
        format.html { render :edit }
        format.json { render json: @clote.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /clotes/1
  # DELETE /clotes/1.json
  def destroy
    @clote.destroy
    respond_to do |format|
      format.html { redirect_to clotes_url, notice: ' Modelo excluído.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_clote
      @clote = Clote.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def clote_params
      params.require(:clote).permit(:model, :size, :price, :description, :category_id, :photo)
    end
end
