class AddPhotoColummsToClote < ActiveRecord::Migration[5.0]

  def self.up
    add_attachment :clotes, :photo
  end

  def self.down
    remove_attachment :clotes, :photo
  end
end
