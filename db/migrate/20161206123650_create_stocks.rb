class CreateStocks < ActiveRecord::Migration[5.0]
  def change
    create_table :stocks do |t|
      t.integer :amount
      t.integer :minimum
      t.references :clote, foreign_key: true
      t.references :measure, foreign_key: true

      t.timestamps
    end
  end
end
