class CreateClotes < ActiveRecord::Migration[5.0]
  def change
    create_table :clotes do |t|
      t.string :model
      t.float :price
      t.string :description
      t.references :category, foreign_key: true
      t.string :photo

      t.timestamps
    end
  end
end
