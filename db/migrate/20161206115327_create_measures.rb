class CreateMeasures < ActiveRecord::Migration[5.0]
  def change
    create_table :measures do |t|
      t.string :size
      t.float :bust
      t.float :waist
      t.float :hip

      t.timestamps
    end
  end
end
