# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

#Users.create(name: 'admin', email: 'admin@lseda.com', password:'$2a$11$9uUWYMZm15c2z.QADWhGsuPowuqI6GTaS6vhUOM.XGqstuBl0lLd.')

User.destroy_all

User.create!([{
  name: 'admin',
  email: 'admin@lseda.com',
  password:'admin1',
  cpf: '00000000000',
  employee: 'y'
}])

User.create!([{
  name: 'user',
  email: 'user@user.com',
  password:'user12',
  cpf: '00000000001',
}])

Measure.create!([{:size => "PP", :bust => 82, :waist => 68, :hip => 84}])
Measure.create!([{:size => "P", :bust => 88, :waist => 74, :hip => 90}])
Measure.create!([{:size => "M", :bust => 96, :waist => 82, :hip => 98}])
Measure.create!([{:size => "G", :bust => 104, :waist => 90, :hip => 106}])
Measure.create!([{:size => "GG", :bust => 112, :waist => 98, :hip => 114}])

Category.create!([{:name => "Camiseta"}])
Category.create!([{:name => "Short"}])
Category.create!([{:name => "Calça"}])
Category.create!([{:name => "Saia"}])
Category.create!([{:name => "Vestido"}])

#Clote.create!([{:model => "Vestido de Bolinha Azul", :price => 60, :description => "Tecido Musseline", :category_id => 5, :photo_file_name => "vestido_bolinha1.jpg", :photo_content_type => "image/jpg"}])
#Clote.create!([{:model => "Vestido de Bolinha Azul", :price => 60, :description => "Tecido Musseline", :category_id => 5}])
#Clote.create!([{:model => "Blusa de Renda",          :price => 80, :description => "Renda Francesa",   :category_id => 1}])
#Clote.create!([{:model => "Jeans Swary",             :price => 100, :description => "Calça Jeans",     :category_id => 3, :photo => File.new("#{Rails.root}/app/assets/images/imageNotAvailable_medium.jpg"),}])

#Stock.create!([{:amount => 37, minimum:7, :clote_id => 1, :measure => 2}])
#Stock.create!([{:amount => 37, minimum:7, :clote => 3, :measure => 3}])
#Stock.create!([{:amount => 37, minimum:7, :clote => 3, :measure => 4}])
#Stock.create!([{:amount => 20, minimum:10, :clote => 1, :measure => 1}])
#Stock.create!([{:amount => 19, minimum:10, :clote => 1, :measure => 2}])
#Stock.create!([{:amount => 28, minimum:10, :clote => 1, :measure => 3}])
#Stock.create!([{:amount => 26, minimum:10, :clote => 1, :measure => 5}])
#Stock.create!([{:amount => 32, minimum:8, :clote => 2, :measure => 1}])
#Stock.create!([{:amount => 32, minimum:8, :clote => 2, :measure => 2}])
#Stock.create!([{:amount => 32, minimum:8, :clote => 2, :measure => 3}])
#Stock.create!([{:amount => 32, minimum:8, :clote => 2, :measure => 4}])
#Stock.create!([{:amount => 32, minimum:8, :clote => 2, :measure => 5}])


p "Created #{User.count} users, #{Measure.count} measures, #{Category.count} categorys."
